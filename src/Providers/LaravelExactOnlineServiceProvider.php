<?php

namespace PendoNL\LaravelExactOnline\Providers;

use Illuminate\Support\ServiceProvider;
use PendoNL\LaravelExactOnline\LaravelExactOnline;

class LaravelExactOnlineServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');

        $this->loadViewsFrom(__DIR__.'/../views', 'laravelexactonline');

        $this->publishes([
            __DIR__.'/../views' => base_path('resources/views/vendor/laravelexactonline')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias(LaravelExactOnline::class, 'laravel-exact-online');

        $this->app->singleton('Exact\Connection', function () {
            if ($tenant = \App\Domains\Tenants\Models\Tenant::current()) {
                $config = LaravelExactOnline::loadConfig();

                if (!$accountancy = $tenant->accountancy) {
                    abort(403);
                }

                $accountancyConfig = $accountancy->config;

                $connection = new \Picqer\Financials\Exact\Connection();
                $connection->setRedirectUrl(route('exact.callback'));
                $connection->setExactClientId($accountancyConfig['clientId']);
                $connection->setExactClientSecret($accountancyConfig['clientSecret']);
                $connection->setBaseUrl('https://start.exactonline.nl');

                if (!empty($accountancyConfig['division'])) {
                    $connection->setDivision($accountancyConfig['division']);
                }
                if (!empty($accountancyConfig['authorisationCode'])) {
                    $connection->setAuthorizationCode($accountancyConfig['authorisationCode']);
                }
                if (!empty($accountancyConfig['accessToken'])) {
                    $connection->setAccessToken($accountancyConfig['accessToken']);
                }
                if (!empty($accountancyConfig['refreshToken'])) {
                    $connection->setRefreshToken($accountancyConfig['refreshToken']);
                }
                if (!empty($accountancyConfig['tokenExpires'])) {
                    $connection->setTokenExpires($accountancyConfig['tokenExpires']);
                }

                $connection->setTokenUpdateCallback('\PendoNL\LaravelExactOnline\LaravelExactOnline::tokenUpdateCallback');

                try {
                    if (isset($tenantCustomer->exact_authorisationCode)) {
                        $connection->connect();
                    }
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $connection->setAccessToken(null);
                    $connection->setRefreshToken(null);
                    $connection->connect();
                } catch (\Exception $e) {
                    throw new \Exception('Could not connect to Exact: ' . $e->getMessage());
                }

                $tenantCustomer->exact_accessToken = serialize($connection->getAccessToken());
                $tenantCustomer->exact_refreshToken = $connection->getRefreshToken();
                $tenantCustomer->exact_tokenExpires = $connection->getTokenExpires();

                LaravelExactOnline::storeConfig($tenantCustomer);

                return $connection;
            } else {
                die('No tenant customer');
            }
        });
    }
}
