<?php

namespace PendoNL\LaravelExactOnline;

use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Picqer\Financials\Exact\Connection;

class LaravelExactOnline
{
    private $connection = [];

    /**
     * LaravelExactOnline constructor.
     */
    public function __construct()
    {
        $this->connection = app()->make('Exact\Connection');
    }

    /**
     * Magically calls methods from Picqer Exact Online API
     *
     * @param $method
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($method, $arguments)
    {
        if(substr($method, 0, 10) == "connection") {

            $method = lcfirst(substr($method, 10));

            call_user_func([$this->connection, $method], implode(",", $arguments));

            return $this;

        } else {

            $classname = "\\Picqer\\Financials\\Exact\\" . $method;

            if(!class_exists($classname)) {
                throw new \Exception("Invalid type called");
            }

            return new $classname($this->connection);

        }

    }

    public static function tokenUpdateCallback (Connection $connection) {
        $config = self::loadConfig();

        $config['accessToken'] = serialize($connection->getAccessToken());
        $config['refreshToken'] = $connection->getRefreshToken();
        $config['tokenExpires'] = $connection->getTokenExpires();

        self::storeConfig($config);
    }

    public static function  loadConfig()
    {
        $tenant = \App\Domains\Tenants\Models\Tenant::current();
        if ($accountancy = $tenant->accountancy) {
            return $accountancy->config;
        } else {
            die('No tenant customer');
        }
    }

    public static function storeConfig($config)
    {
        $tenant = \App\Domains\Tenants\Models\Tenant::current();
        if ($tenant->accountancy) {
            $tenant->accountancy->config = $config;
            $tenant->accountancy->save();
        } else {
            die('No tenant customer');
        }
    }

}
